import React, { useState, useEffect } from "react";
import MaterialTable from 'material-table';

const EmpTable = () => {
  const [emp, setEmp] = useState({});
  useEffect(() => {
    getEmployee();
  }, []);

  const editEmp = async (name, lname, data) => {
    const rawResponse = await fetch(process.env.REACT_APP_API_BASE_URL + '/editEmployee', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(
        {
          "name": name,
          "lname": lname,
          "new_data": data
        }
      )
    });
    return await rawResponse.json();
  }

  const insertEmp = async (data) => {
    const rawResponse = await fetch(process.env.REACT_APP_API_BASE_URL + '/insertEmployee', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(
        {
          "data": data
        }
      )
    });
    return await rawResponse.json();
  }

  const delEmp = async (name, lname) => {
    const rawResponse = await fetch(process.env.REACT_APP_API_BASE_URL + '/delEmployee', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(
        {
          "name": name,
          "lname": lname
        }
      )
    });
    return await rawResponse.json();
  }

  const getEmployee = async () => {
    const rawResponse = await fetch(process.env.REACT_APP_API_BASE_URL + '/getEmployee', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(
        {
          "name": "",
          "lname": ""
        }
      )
    });
    const data = await rawResponse.json();
    if (data.data) {
      let header = [{ title: 'ชื่อ*', field: 'name' },
      { title: 'นามสกุล*', field: 'lname' },
      { title: 'อายุ', field: 'age', type: "numeric"},
      { title: 'เงินเดือน', field: 'salary', type: "numeric" }
      ]

      setEmp({ columns: header, data: data.data });
    } else {
      setEmp([]);
    }
  };

  const validate = (data) =>{
    if( (!data.name || !data.lname) || (data.name.trim() == "" || data.lname.trim() == "") )
      return "กรุณาใส่ชื่อแล้วนามสกุล";
    return "";
  }
  return (
    <div >
      <MaterialTable
        title="ตารางพนักงาน"
        columns={emp.columns}
        data={emp.data}
        localization={{
          toolbar: {
            searchTooltip: "ค้นหา",
            searchPlaceholder: "ค้นหา",

          },
          pagination: {
            labelDisplayedRows: '{from}-{to} จาก {count}',
            labelRowsSelect: "แถว"
          },
          header: {
            actions: ''
          },
          body: {
            emptyDataSourceMessage: (
              <div >
                กรุณากดปุ่ม + เพิ่มรายการสินค้า
              </div>
            ),
            editRow: {
              deleteText: (
                <div >
                  คุณต้องการลบพนักงานคนนี้ใช่หรือไม่
                </div>
              ),
              cancelTooltip: "ยกเลิก",
              saveTooltip: "บันทึก",
            },
            addTooltip: "เพิ่ม",
            deleteTooltip: "ลบ",
            editTooltip: "แก้ไข",
          },
        }}
        editable={{
          onRowAdd: (newData) =>
            new Promise(async (resolve) => {
              let v = validate(newData);
              if(v != ""){
                alert(v);
                resolve();
                return;
              }
              let data = await insertEmp(newData);
              if (data.status.statusCode == "0") {
                setTimeout(() => {
                  resolve();
                  setEmp((prevState) => {
                    const data = [...prevState.data];
                    data.push(newData);
                    return { ...prevState, data };
                  });
                }, 600);
              } else {
                alert("ไม่สามารถบันทึกได้ " + data.status.Message);
                resolve();
              }
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise(async (resolve) => {
              let v = validate(newData);
              if(v != ""){
                alert(v);
                resolve();
                return;
              }
              let data = await editEmp(oldData.name, oldData.lname, newData);
              if (data.status.statusCode == "0") {
                setTimeout(() => {
                  resolve();
                  if (oldData) {
                    setEmp((prevState) => {
                      const data = [...prevState.data];
                      data[data.indexOf(oldData)] = newData;
                      return { ...prevState, data };
                    });
                  }
                }, 600);
              } else {
                alert("ไม่สามารถบันทึกได้ " + data.status.Message);
                resolve();
              }

            }),
          onRowDelete: (oldData) =>
            new Promise(async (resolve) => {
              let data = await delEmp(oldData.name, oldData.lname);
              if (data.status.statusCode == "0") {
                setTimeout(() => {
                  resolve();
                  setEmp((prevState) => {
                    const data = [...prevState.data];
                    data.splice(data.indexOf(oldData), 1);
                    return { ...prevState, data };
                  });
                }, 600);
              } else {
                alert("ไม่สามารถลบได้ " + data.status.Message);
                resolve();
              }

            }),
        }}
      />


    </div>
  )
}


export default EmpTable;